<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('meta_title')|E家管理后台</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/simditor.css" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/admin') }}">
                管理后台
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">登陆</a></li>
                    <li><a href="{{ url('/register') }}">注册</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>


<div class="container">
    @include('errors._messages')

    <div class="row">
        {{--左侧菜单栏--}}
        <div class="col-md-2">
            <div class="list-group">
                <a href="{{url('/configuration/editSaleStage')}}" id="conf_sale_stage" class="list-group-item">配置置业阶段</a>
                <a href="{{url('/configuration/editEstateType')}}" id="conf_estate_type" class="list-group-item">配置物业类型</a>
                <a href="{{url('/configuration/editAgentCareer')}}" id="conf_agent_career" class="list-group-item">配置置业等级</a>
                <a href="{{url('/configuration/editActivityRule')}}" id="conf_activity_rule" class="list-group-item">配置活动规则</a>
                <a href="{{url('/admin')}}" class="list-group-item" id="agent">经纪人列表 </a>
                <a href="{{route('property.index')}}" id="property" class="list-group-item">楼盘列表</a>
                <a href="{{route('progress.index')}}" id="progress" class="list-group-item">购房进度</a>
                <a href="{{route('client.index')}}" id="client" class="list-group-item">顾客列表</a>
                <a href="{{route('image.index')}}" id="image" class="list-group-item">云图片</a>
            </div>
        </div>
        <div class="col-md-10">
            @yield('content')
        </div>
    </div>

</div>

        <!-- Scripts -->
<script src="/js/app.js"></script>
<script src="/js/module.js"></script>
<script src="/js/hotkeys.js"></script>
<script src="/js/uploader.js"></script>
<script src="/js/simditor.js"></script>
    @yield('script')
</body>
</html>
