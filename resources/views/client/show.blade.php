<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="/static/css/index.css" />
</head>
<body>
<section class="setbacks">
    <div class="setbacks-top">
        <p>
            <span>购房进度</span>
        </p>
    </div>
    <div class="setbacks-ul">
        <p class="setbacks-ul-title">{{$client->recommend_property}}</p>

        @if(empty($client->progresses))
            <p>暂时没有顾客进度信息!</p>
        @else
            <ul>
                @foreach( $client->progresses as $progress)
                <li>
                    <p>{{$progress->stage}}</p>
                    <p>{{$progress->stage_time}}</p>
                </li>
                @endforeach

            </ul>
        @endif



    </div>
</section>

<script src="/static/js/jquery-2.1.3.min.js"></script>
<script src="/static/js/flexible.js"></script>
</body>
</html>
