<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="format-detection" content="email=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <title>全民经纪人</title>

    <link rel="stylesheet" href="/static/css/index.css"/>
</head>
<body>
<section class="custom">
    <div class="custom-top">
        <p>
            <em><img src="/static/img/custom-pic.png" alt=""></em>
            <span>我的客户</span>
        </p>

        <div class="custom-right">
            <p>{{$clients->count()+$dealers->count()}}</p>
            <p>推荐中总数</p>
        </div>
    </div>
    <div class="custom-tab">
        <span class="custom-tab-active"><a href="javascript:;">推荐中（{{$clients->count()}}）</a></span>
        <!-- <span><a href="javascript:;">购房进度（2）</a></span> -->
        <span class="tab-token"><a href="javascript:;">已成交（{{$dealers->count()}}）</a></span>
    </div>
    <div class="custom-search">
        <p>
            <em><img src="static/img/search.png" alt=""></em>
            <input type="text">
        </p>
    </div>
    {{--推荐中 客户--}}

    <div class="custom-con" id="customIng">
        <ul>
            @foreach($clients as $client)
                <li>
                    <div class="custom-con-l">
                        <p>{{$client->name}}</p>
                    </div>
                    <div class="custom-con-c">
                        <div class="custom-con-t">{{$client->phone}}&nbsp;&nbsp;推荐中</div>
                        <p>推荐楼盘：{{$client->recommend_property}}</p>

                        <p>接收置业顾问:{{$client->user->name}}</p>
                    </div>
                    <div class="custom-con-r">
                        <a href="tel:{{$client->phone}}">
                            <img src="/static/img/phone.png" alt=""  width="50" height="50">
                        </a>
                        @if($client->progresses->count())
                            <a href="{{route('client.show',$client)}}">
                                购房进度
                            </a>
                        @endif
                    </div>
                </li>

            @endforeach

        </ul>
    </div>

    {{--已成交 客户--}}
    <div class="custom-con" id="customDone">
        <ul>
            @foreach($dealers as $client)
                <li>
                    <div class="custom-con-l">
                        <p>{{$client->name}}</p>
                    </div>
                    <div class="custom-con-c">
                        <div class="custom-con-t">{{$client->phone}}&nbsp;&nbsp;推荐中</div>
                        <p>推荐楼盘：{{$client->recommend_property}}</p>

                        <p>接收置业顾问:{{$client->user->name}}</p>
                    </div>
                    <div class="custom-con-r">
                        <a href="tel:{{$client->phone}}">
                            <img src="/static/img/phone.png" alt="" width="50" height="50">
                        </a>
                        @if($client->progresses->count())
                            <a href="{{route('client.show',$client)}}">
                                购房进度
                            </a>
                        @endif
                    </div>
                </li>

            @endforeach

        </ul>
    </div>
    <div class="custom-btn">
        <p class="sub"><a href="javascript:history.go(-1);">返回首页</a></p>
    </div>
</section>

<script src="/static/js/jquery-2.1.3.min.js"></script>
<script src="/static/js/flexible.js"></script>
<script>
$(function(){
    $('#customDone').hide();
    $('.custom-tab span').click(function(){
        $('.custom-tab span').removeClass('custom-tab-active');
        $(this).addClass('custom-tab-active');
        if($(this).hasClass('tab-token')){
            $('#customIng').hide();
            $('#customDone').show();
        }else{
            $('#customDone').hide();
            $('#customIng').show();
        }
    })
})
</script>
</body>
</html>
