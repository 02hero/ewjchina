<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="/static/css/index1.css" />
</head>
<body>
<section class="register">
    <div class="register-top">
        <p>
            <em><img src="/static/img/register-pic.png" alt=""></em>
            <span>推荐客户</span>
        </p>
    </div>
    <div class="register-tab">
        <span class="register-tab-active" data-type="1">按楼盘推荐</span>
        <span data-type="2">按需求推荐</span>
    </div>
    <div class="register-form">
        <ul>
            <li>
                <span>客户姓名</span>
                <input type="text" id="name">
            </li>
            <li>
                <span>手机号</span>
                <input type="text" id="phone" type="number" max="19999999999" min="10000000000">
            </li>
            <li>
                <span>性别</span>
                <select id="gender">
                    <option value="男">男</option>
                    <option value="女">女</option>
                </select>
            </li>
            <li class="register-form-1">
                <span>推荐楼盘</span>
                <select id="property_id">
                    @foreach($properties as $property)
                        <option value="{{$property->id}}">{{$property->title}}</option>
                    @endforeach
                </select>
            </li>
            <li class="register-form-2">
                <span>置业区域</span>
                <input type="text" id="intention_area">
            </li>
            <li class="register-form-2">
                <span>物业类型</span>
                <select id="intention_type">
                    @foreach($propertyTypes as $type)
                        <option value="{{$type}}">{{$type}}</option>
                    @endforeach
                </select>
            </li>
            <li class="register-form-2">
                <span>意向房型</span>
                <input type="text" id="intention_apartment">
            </li>
            <li class="register-form-2">
                <span>意向总价</span>
                <input type="text" id="intention_price">
            </li>
            <li>
                <span>备注</span>
                <input type="text" id="remark">
            </li>
            <li class="sub">
                <p id="btn">推荐</p>
            </li>
        </ul>
    </div>
</section>

<script src="/static/js/jquery-2.1.3.min.js"></script>
<script src="/static/js/flexible.js"></script>
<script src="/static/js/register.js"></script>
<script>
    $("#btn").click(function(){
        var para = {
            name : $('#name').val(),
            phone : $('#phone').val(),
            gender : $('#gender').val(),
            property_id : $('#property_id').val(),
            intention_price : $('#intention_price').val(),
            intention_apartment : $('#intention_apartment').val(),
            intention_type : $('#intention_type').val(),
            intention_area : $('#intention_area').val(),
            remark : $('#remark').val(),
            _token : "{{csrf_token()}}"
        }

        $.ajax({
            url:"{{route('client.store')}}",
            type: "post",
            dataType: "json",
            data: para,
            success: function (data) {
                window.location.href='{{url('myClients')}}';
            },
            error: function (data) {
                alert('电话号码,和姓名是必须,请重新填写')

            }
        });
    });
</script>
</body>
</html>
