@extends('layouts.admin')

@section('meta_title','编辑顾客信息')

@section('content')



    <h2 class="text-center text-success"> 编辑顾客信息</h2>

    {!! Form::model($client, ['route' => ['client.update',$client], 'method' => 'put']) !!}
    {!! Form::text('id', $client->id, ['class' => 'form-control hidden']) !!}
    {!! Form::label('name', '姓名', ['class' => 'control-label']) !!}
    {!! Form::text('name',null, ['class' => 'form-control']) !!}

    {!! Form::label('phone', '手机号码', ['class' => 'control-label']) !!}
    {!! Form::text('phone',null, ['class' => 'form-control']) !!}

    {!! Form::label('gender', '性别', ['class' => 'control-label']) !!}
    {!! Form::select('gender', ['男'=>'男','女'=>'女'] , null , ['class' => 'form-control']) !!}

    {!! Form::label('intentions_price', '意向价格', ['class' => 'control-label']) !!}
    {!! Form::text('intention_price',null, ['class' => 'form-control']) !!}


    {!! Form::label('intention_area', '意向区域', ['class' => 'control-label']) !!}
    {!! Form::text('intention_area',null, ['class' => 'form-control']) !!}


    {!! Form::label('intention_type', '意向户型', ['class' => 'control-label']) !!}
    {!! Form::text('intention_type',null, ['class' => 'form-control']) !!}


    {!! Form::label('intention_apartment', '意向住宅', ['class' => 'control-label']) !!}
    {!! Form::text('intention_apartment',null, ['class' => 'form-control']) !!}


    {!! Form::label('remark', '备注', ['class' => 'control-label']) !!}
    {!! Form::text('remark',null, ['class' => 'form-control']) !!}
    @if($client->is_finished)
        <label>
            {!! Form::checkbox('is_finished','0', true ) !!}
            <span class="text-success">客户是否成交</span>
        </label>
    @else
        <label>
            {!! Form::checkbox('is_finished','1', false ) !!}
            <span class="text-success">客户是否成交(成交打钩,未成交不打勾)</span>
        </label>
    @endif








    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
    {!! Form::close() !!}


@endsection

@section('script')
    <script>
        $('#client').addClass("active");
    </script>
@endsection


