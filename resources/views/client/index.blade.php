@extends('layouts.admin')


@section('meta_title','顾客列表')


@section('content')
    <div class="well-lg">
        <h2 class="text-center text-success">顾客列表</h2>
        <table class="table table-striped table-hover table-responsive table-condensed table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>职业姓名</th>
                <th>顾客名字</th>
                <th>手机</th>
                <th>性别</th>
                <th>意向价格</th>
                <th>意向价格</th>
                <th>意向区域</th>
                <th>意向房型</th>
                <th>意向楼盘</th>
                <th>备注说明</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client->id}}</td>
                    <td>{{$client->user->name}}</td>
                    <td>{{$client->name}}</td>
                    <td>{{$client->phone}}</td>
                    <td>{{$client->gender}}</td>
                    <td>{{$client->intention_price}}</td>
                    <td>{{$client->intention_area}}</td>
                    <td>{{$client->intention_type}}</td>
                    <td>{{$client->intention_apartment}}</td>
                    <td>{{$client->recommend_property}}</td>
                    <td>{{$client->remark}}</td>
                    <td>
                        @if($client->progresses->count())
                            <a class="btn btn-success" href="{{route('client.show',$client)}}">进度</a>

                        @endif
                        <a class="btn btn-info" href="{{route('client.edit',$client)}}">编辑</a>
                        {!! Form::model($client, ['route' => ['client.destroy', $client], 'method' => 'delete']) !!}
                        {!! Form::submit('删除', ['class' => 'form-control btn-danger btn-xs']) !!}
                        {!! Form::close() !!}

                    </td>
                </tr>
            @endforeach

            </tbody>


        </table>

    </div>
@endsection


@section('script')
    <script>
        $(function(){

            $('#client').addClass("active");
        });
    </script>
@endsection