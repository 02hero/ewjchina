@extends('layouts.admin')


@section('meta_title','楼盘列表')


@section('content')
    <div class="well-lg">
        <h2 class="text-center text-success"> 楼盘列表</h2>
        <p class="text-right"><a class="btn btn-success" href="{{route('property.create')}}">添加楼盘信息</a> </p>
        <table class="table table-striped table-hover table-responsive table-condensed table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>名称</th>
                <th>详情</th>
                <th>封面图片地址</th>
                <th>多张图片</th>
                <th>城市/区域</th>
                <th>地址</th>
                <th>特色买点</th>
                <th>单价</th>
                <th>佣金</th>
                <th>佣金描述</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            @foreach($properties as $property)
                <tr>
                    <td>{{$property->id}}</td>
                    <td>{{$property->title}}</td>
                    <td>{{$property->detail}}</td>
                    <td>
                        <img src="{{$property->coverage_h100}}" class="img-responsive" alt="Responsive image">
                    </td>
                    <td>{{$property->images}}</td>
                    <td>{{$property->city_area}}</td>
                    <td>{{$property->address}}</td>
                    <td>{{$property->feature}}</td>
                    <td>{{$property->price}}</td>
                    <td>{{$property->commission}}元</td>
                    <td>{{$property->commission_detail}}</td>
                    <td>
                        <a class="btn btn-primary" href="{{route('property.edit',$property)}}">编辑</a>
                        <a href="{{url('property/uploadImage',$property)}}" class="btn btn-block btn-warning">上传封面</a>

                        {!! Form::model($property, ['route' => ['property.destroy', $property], 'method' => 'delete']) !!}
                        {!! Form::submit('删除', ['class' => 'form-control btn-danger']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach

            </tbody>


        </table>

    </div>
@endsection


@section('script')
    <script>
        $(function(){

            $('#property').addClass("active");
        });
    </script>
@endsection