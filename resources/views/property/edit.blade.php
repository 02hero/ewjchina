@extends('layouts.admin')


@section('meta_title','编辑楼盘信息')


@section('content')

                <div class="well-sm">


                    {!! Form::open(['route' => 'property.store', 'method' => 'post']) !!}
                    {!! Form::text('id', $property->id, ['class' => 'form-control hidden']) !!}
                    {!! Form::label('title', '楼盘名称', ['class' => 'control-label','placeholder'=>'必填']) !!}
                    {!! Form::text('title', $property->title, ['class' => 'form-control']) !!}

                    {!! Form::label('detail', '楼盘详情', ['class' => 'control-label']) !!}
                    {!! Form::textarea('detail', $property->detail, ['class' => 'form-control','rows'=>'2']) !!}

                    {{--{!! Form::label('coverage_img', '封面图片', ['class' => 'control-label']) !!}--}}
                    {{--{!! Form::text('coverage_img', $property->detail, ['class' => 'form-control']) !!}--}}

                    {{--{!! Form::label('images', '其他图片', ['class' => 'control-label']) !!}--}}
                    {{--{!! Form::text('images', $property->images, ['class' => 'form-control']) !!}--}}

                    {!! Form::label('city_area', '城市/区域', ['class' => 'control-label']) !!}
                    {!! Form::text('city_area', $property->city_area, ['class' => 'form-control']) !!}

                    {!! Form::label('address', '地址', ['class' => 'control-label']) !!}
                    {!! Form::text('address', $property->address, ['class' => 'form-control']) !!}

                    {!! Form::label('feature', '买点/特色.房型', ['class' => 'control-label']) !!}
                    {!! Form::text('feature', $property->feature, ['class' => 'form-control']) !!}

                    {!! Form::label('price', '单价', ['class' => 'control-label']) !!}
                    {!! Form::text('price', $property->price, ['class' => 'form-control']) !!}


                    {!! Form::label('commission', '佣金金额(单位元) 金额', ['class' => 'control-label text-danger']) !!}
                    {!! Form::number('commission', $property->commission, ['class' => 'form-control','placeholder'=>'1000','min'=>'1','max'=>'10000']) !!}

                    {!! Form::label('commission_detail', '佣金文字说明', ['class' => 'control-label']) !!}
                    {!! Form::textarea('commission_detail', $property->commission_detail, ['class' => 'form-control','rows'=>'2']) !!}


                    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
                    {!! Form::close() !!}
                </div>

                @if($property->coverage_img)
                    <h3 class="text-center lead">楼盘封面照片</h3>
                    <img src="{{$property->coverage_url}}" class="img-responsive" alt="Responsive image">
                    <br>
                @endif





@endsection

@section('script')
    <script>
        $(function(){
            $('#property').addClass("active");
        });

        $('#btnImage').click(function(){

            var para = {
                image:$('#coverage_img').val(),
                propertyId:"{{$property->id}}",
                _token:"{{csrf_token()}}"
            };
            $.ajax({
                url: '{{url('admin/uploadCoverageImageForProperty')}}',
                type: 'POST',
                cache: false,
                data: para,
                processData: false,
                contentType: false
            }).done(function(res) {
            });


        });
    </script>
@endsection