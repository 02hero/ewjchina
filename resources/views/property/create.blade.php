@extends('layouts.admin')


@section('meta_title','添加楼盘信息')


@section('content')
    <h2 class="text-center">添加楼盘信息</h2>
    <div class="well-lg">
        {!! Form::open(['route' => 'property.store', 'method' => 'post']) !!}

        {!! Form::label('title', '楼盘名称(必填)', ['class' => 'control-label text-danger','placeholder'=>'万科城花']) !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}

        {!! Form::label('detail', '楼盘详情', ['class' => 'control-label']) !!}
        {!! Form::textarea('detail', null, ['class' => 'form-control','rows'=>'2']) !!}

        {{--{!! Form::label('coverage_img', '封面图片', ['class' => 'control-label']) !!}--}}
        {{--{!! Form::text('coverage_img', null, ['class' => 'form-control']) !!}--}}

        {{--{!! Form::label('images', '其他图片', ['class' => 'control-label']) !!}--}}
        {{--{!! Form::text('images', null, ['class' => 'form-control']) !!}--}}

        {!! Form::label('city_area', '城市/区域', ['class' => 'control-label text-danger','placeholder'=>'光谷高新 关山']) !!}
        {!! Form::text('city_area', null, ['class' => 'form-control']) !!}

        {!! Form::label('address', '地址', ['class' => 'control-label']) !!}
        {!! Form::text('address', null, ['class' => 'form-control']) !!}

        {!! Form::label('feature', '买点/特色', ['class' => 'control-label']) !!}
        {!! Form::text('feature', null, ['class' => 'form-control']) !!}

        {!! Form::label('price', '单价(必填)', ['class' => 'control-label text-danger']) !!}
        {!! Form::text('price', null, ['class' => 'form-control']) !!}


        {!! Form::label('commission', '金额(必填) 元', ['class' => 'control-label text-danger']) !!}
        {!! Form::number('commission',1, ['class' => 'form-control','min'=>'1','max'=>'10000']) !!}

        {!! Form::label('commission_detail', '佣金详情(必填)文字说明', ['class' => 'control-label text-danger']) !!}
        {!! Form::textarea('commission_detail', null, ['class' => 'form-control','rows'=>'2']) !!}

        {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
        {!! Form::close() !!}
    </div>
@endsection



@section('script')
    <script>
        $(function(){
            $('#property').addClass("active");
        });
    </script>
@endsection