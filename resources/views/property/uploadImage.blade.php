@extends('layouts.admin')


@section('meta_title','上传楼盘图片')


@section('content')

                    <h2 class="text-center text-success"> {{$property->title}}</h2>

                    {!! Form::open(['url' => 'property/storeImage', 'method' => 'post' ,'files'=>true]) !!}

                        {!! Form::label('image', '上传图片', ['class' => 'control-label text-danger']) !!}
                        {!! Form::file('image', ['class' => 'form-control','accept'=>'image/*']) !!}
                        {!! Form::text('property_id',$property->id, ['class' => 'form-control hidden']) !!}
                        {!! Form::submit('上传楼盘封面照片', ['class' => 'form-control btn-success']) !!}
                    {!! Form::close() !!}

@endsection

@section('script')
    <script>
        $(function(){
            $('#property').addClass("active");
        });


    </script>
@endsection