<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /> -->
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/index.css" />
</head>
<body>
<section class="index">
    <div class="index-top">
        <div class="index-pic">
            <img src="{{$user->wechat_avatar}}" alt="">
        </div>
        <div class="index-top-text">
            <p>{{$user->name}}</p>
            <p><em>￥{{$commission}}</em>&nbsp;已推荐{{$user->clients()->count()}}人</p>
        </div>
        <div class="index-top-a">
            <a href="{{route('client.create')}}">
                <img src="static/img/tuijiankehu.png" alt="">
            </a>
            <a href="{{$shareURL}}">
                <img src="static/img/share-juan.png" alt="">
            </a>
        </div>
    </div>
    <div class="index-tab">
        <p>
            <a href="{{url('myClients')}}"><img src="static/img/index-icon-1.png" alt="">我的客户</a>
        </p>
        <p>
            <a href="{{url('commission')}}"><img src="static/img/index-icon-2.png" alt="">我的佣金</a>
        </p>
        <p>
            <a href="rule"><img src="static/img/index-icon-3.png" alt="">活动细则</a>
        </p>
    </div>
    <div class="index-con">
        <ul>
            @foreach($properties as $property)
                <li>
                    <div class='index-con-img'>
                        <img src="{{config('app.QNURL').$property->coverage_img}}" alt="">
                    </div>
                    <div class="index-con-p">
                        <div class="index-con-p-title">{{$property->title}}</div>
                        <p class="index-con-p-price">价格：{{$property->price}}</p>
                        <p>佣金：{{$property->commission}}元</p>
                        <p>区域：{{$property->city_area}}</p>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</section>
<!-- <script src="js/rem.js"></script> -->

<script src="static/js/jquery-2.1.3.min.js"></script>
<script src="static/js/flexible.js"></script>
</body>
</html>
