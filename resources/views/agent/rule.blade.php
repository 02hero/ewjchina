<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/index.css" />
</head>
<body>
<section class="rule">
    <div class="rule-top">
        <p>
            <span>活动规则</span>
        </p>
    </div>
    <div class="rule-ul">
        {{--<ul>--}}
            {{--<li>--}}
                {{--<p>黄石公园</p>--}}
                {{--<p>洋房-8000元/套</p>--}}
                {{--<p>高层-5000元/套</p>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<p>联润商贸城</p>--}}
                {{--<p>商铺-8000元/套</p>--}}
                {{--<p>高层-5000元/套</p>--}}
            {{--</li>--}}
        {{--</ul>--}}

        {!! $conf->value !!}

    </div>
    {{--<div class="rule-con">--}}
        {{--<p>推介及奖金领取流程：</p>--}}
        {{--<p>1、推介的客户为营销中心从未接待和登记过的来访客户。</p>--}}
        {{--<p>2、介绍客户签约成功并全款到位。</p>--}}
        {{--<p>3、客户签约成功并全款到位后30日内发放全额奖金。（税金自理）--}}
            {{--活动截止时间：2016年12月31日</p>--}}
        {{--<p>e家经济人有权在法律允许的范围内保留对本次活动的最终解释权。</p>--}}
    {{--</div>--}}
    {{--<div class="rule-btn">--}}
        {{--<a href="javascript:history.go(-1)"><p class="sub">返回首页</p></a>--}}
    {{--</div>--}}
</section>

<script src="static/js/jquery-2.1.3.min.js"></script>
<script src="static/js/flexible.js"></script>
</body>
</html>
