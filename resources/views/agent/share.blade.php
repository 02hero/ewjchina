<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="format-detection" content="telephone=no" />
        <meta name="format-detection" content="email=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <title>全民经纪人</title>

        <link rel="stylesheet" href="static/css/index.css" />
    </head>
    <body>
        <section class="share">
            <div class="share-top">
                <p>
                    <em><img src="static/img/share-pic.png" alt=""></em>
                    <span>分享优惠劵</span>
                </p>
            </div>
            <div class="share-form">
                <ul>
                    <li>
                        <select name="property">
                            @foreach($properties as $property)
                            <option value="{{$property->title}}">{{$property->title}}</option>
                            @endforeach
                        </select>
                    </li>
                    <li class="textarea">
                        <textarea name="discount_info" placeholder="您的朋友可以享受的分享优惠"></textarea>
                    </li>
                    <li class="sub">
                        <p>生成优惠劵</p>
                    </li>
                </ul>
            </div>
        </section>

        <script src="static/js/jquery-2.1.3.min.js"></script>
        <script src="static/js/flexible.js"></script>
        <script type="text/javascript">
            $(function(){
                $('.sub p').on('click',function(){
                    window.location.href="{{url('coupon')}}";
                });
            });
        </script>
    </body>
</html>
