<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/index.css" />
</head>
<body>
<section class="coupon">
    <div class="coupon-top">
        <p>
            <span>优惠劵</span>
        </p>
    </div>
    <div class="coupon-con">
        <ul>
            <li class="coupon-li-1">
                <span>黄石公园</span>
            </li>
            <li class="coupon-li-2">
                <img src="static/img/erweima.png" alt="">
            </li>
        </ul>
    </div>
</section>

<script src="static/js/jquery-2.1.3.min.js"></script>
<script src="static/js/flexible.js"></script>
</body>
</html>
