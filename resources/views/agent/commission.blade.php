<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/index.css" />
</head>
<body>
<section class="pirce">
    <div class="pirce-top">
        <p>
            <em><img src="static/img/index-icon-2.png" alt=""></em>
            <span>我的佣金</span>
        </p>
    </div>
    <div class="pirce-tab">
        <div class="pirce-tab-con">
            <em class="em-1">{{$commission}}</em>
            <p>应结佣金</p>
        </div>
        <div class="pirce-tab-con">
            <em>{{$paid_commission}}</em>
            <p>已结佣金</p>
        </div>
    </div>
    <!-- <div class="price-btn">
        <p class="sub">绑定银行卡</p>
        <p>没有佣金信息</p>
    </div> -->
</section>

<script src="static/js/jquery-2.1.3.min.js"></script>
<script src="static/js/flexible.js"></script>
</body>
</html>
