<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/swiper-3.3.1.min.css" />
    <link rel="stylesheet" href="static/css/animate.min.css" />
    <link rel="stylesheet" href="static/css/index.css" />
</head>

<body class="load-bg">
<section class="load">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="load-logo_index ani" swiper-animate-effect="fadeInDown" swiper-animate-duration="0.5s" swiper-animate-delay="0s">
                    <img src="static/img/logo.png" alt="">
                </div>
                <div class="load-bb ani" swiper-animate-effect="fadeInDown" swiper-animate-duration="0.5s" swiper-animate-delay="0s">
                    <img src="static/img/index-bb.png" alt="">
                </div>
                <div class="load-dd ani" swiper-animate-effect="fadeInDown" swiper-animate-duration="0.5s" swiper-animate-delay="0s">
                    <img src="static/img/load-ll.png" alt="">
                </div>
                <div class="load-cc ani" swiper-animate-effect="tada" swiper-animate-duration="0.8s" swiper-animate-delay="0.3s">
                    <img src="static/img/load-cc.png" alt="">
                </div>
                <div class="load-reg ani" swiper-animate-effect="bounceInUp" swiper-animate-duration="0.8s" swiper-animate-delay="0.6s">
                    <a href="{{$url}}"><img src="static/img/reg.png" alt=""></a>
                </div>
                <div class="load-reg ani" swiper-animate-effect="bounceInUp" swiper-animate-duration="0.8s" swiper-animate-delay="0.6s">
                    <a href="{{$url}}"><img src="static/img/load-dl.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/static/js/jquery-2.1.3.min.js"></script>
<script src="/static/js/flexible.js"></script>
<script charset="utf-8" src="/static/js/swiper-3.3.1.min.js"></script>
<script charset="utf-8" src="/static/js/swiper.animate1.0.2.min.js"></script>
<script>
    $(document).ready(function() {
        var mySwiper = new Swiper('.swiper-container', {
            onInit: function(swiper) { //Swiper2.x的初始化是onFirstInit
                swiperAnimateCache(swiper); //隐藏动画元素
                swiperAnimate(swiper); //初始化完成开始动画
            },
            onSlideChangeEnd: function(swiper) {
                swiperAnimate(swiper); //每个slide切换结束时也运行当前slide动画
            }
        })
    })
</script>
</body>

</html>
