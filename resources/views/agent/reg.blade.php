<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <title>全民经纪人</title>

    <link rel="stylesheet" href="static/css/index1.css" />
</head>
<body>
<section class="reg">
    @include('errors._messages')
    <div class="reg-top">
        <p>
            <em><img src="static/img/index-icon-1.png" alt=""></em>
            <span>注册经纪人</span>
        </p>
    </div>
    <div class="reg-tab">
        <p>以下资料务必真是填写</p>
        <em class="reg-diver"></em>
    </div>
    <div class="reg-form">
        <ul>
            <li>
                <select id="type">
                    @foreach($agent_types as $type)
                    <option value="{{$type}}">{{$type}}</option>
                    @endforeach
                </select>
            </li>
            <li>
                <span>公司名称</span>
                <input type="text" id="company">
            </li>
            <li>
                <span>姓名</span>
                <input type="text" id="name">
            </li>
            <li>
                <select id="gender">
                        <option value="男">男</option>
                        <option value="女">女</option>
                </select>
            </li>
            <li>
                <span>手机号</span>
                <input type="text" id="phone" type="number" max="19999999999" min="10000000000">
            </li>
            <li>
                <span>身份证</span>
                <input type="text" id="indentity">
            </li>
            <li class="checkbox">
                <p>
                    <em></em>我已阅读合作协议并同意
                </p>
            </li>
            <li class="sub">
                <p id="submitForm">注册</p>
            </li>
        </ul>
    </div>
</section>

<script src="static/js/jquery-2.1.3.min.js"></script>
<script src="static/js/flexible.js"></script>

<script>


    $("#submitForm").click(function(){
        var para = {
            agent_type : $('#type').val(),
            company : $('#company').val(),
            name : $('#name').val(),
            gender : $('#gender').val(),
            phone : $('#phone').val(),
            identity_id : $('#indentity').val(),
            _token : "{{ csrf_token() }}"
        }


        $.ajax({
            url: "{{url('saveAgentInfo')}}",
            type: "post",
            dataType: "json",
            data: para,
            success: function (data) {
                window.location.href='{{url('agent')}}'; // 跳转到B目录
            },
            error: function (data) {
                alert('填写数据不合法,或者电话号码已经注册,请重新填写')

            }
        });
    });
</script>

</body>
</html>
