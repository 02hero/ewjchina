@extends('layouts.admin')


@section('meta_title','用户列表')


@section('content')

        <h2 class="text-center text-success"> 用户/经纪人列表</h2>
        <table class="table table-striped table-hover table-responsive table-condensed table-bordered">
                <thead>
                <tr>
                    <th>id</th>
                    <th>pid</th>
                    <th>Email</th>
                    <th>微信图像</th>
                    <th>姓名</th>
                    <th>微信名称</th>
                    <th>手机号码</th>
                    <th>身份证号码</th>
                    <th>地址</th>
                    <th>公司</th>
                    <th>已支付佣金</th>
                    <th>操作</th>
                </tr>
                </thead>

                <tbody>
                @foreach($agents as $agent)
                    <tr>
                        <td>{{$agent->id}}</td>
                        <td>{{$agent->pid}}</td>
                        <td>{{$agent->email}}</td>
                        <td><img src="{{$agent->wechat_avatar}}" width="60" height="60"> </td>


                        <td>{{$agent->name}}</td>
                        <td>{{$agent->wechat_name}}</td>
                        <td>{{$agent->phone}}</td>
                        <td>{{$agent->identity_id}}</td>
                        <td>{{$agent->address}}</td>
                        <td>{{$agent->company}}</td>
                        <td>{{$agent->paid_commission}}</td>

                        <td>
                            <a class="btn btn-primary" href="{{url('admin/userEdit',$agent)}}">编辑</a>
                            <a class="btn btn-danger" href="{{url('admin/userDestroy',$agent)}}">删除</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>



                </thead>
            </table>


@endsection



@section('script')
    <script>
        $(function(){
            $('#agent').addClass("active");
        });
    </script>
@endsection