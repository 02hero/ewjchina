@extends('layouts.admin')


@section('meta_title','初始化配置信息')


@section('content')
        <h2 class="lead text-center text-success">{{'昵称'.$user->name.'邮件'.$user->email}}</h2>

        <div class="well-sm">

            {!! Form::open(['url' => '/admin/userUpdate', 'method' => 'post']) !!}
            {!! Form::text('id', $user->id, ['hidden']) !!}

            {!! Form::label('name','昵称', ['class' => 'control-label text-warning']) !!}
            {!! Form::text('name', $user->name, ['class' => 'form-control',]) !!}

            {!! Form::label('company','公司', ['class' => 'control-label text-warning']) !!}
            {!! Form::text('company', $user->company, ['class' => 'form-control',]) !!}

            {!! Form::label('email','email', ['class' => 'control-label text-warning']) !!}
            {!! Form::text('email', $user->email, ['class' => 'form-control',]) !!}

            {!! Form::label('paid_commission','设置已经支付的佣金', ['class' => 'control-label text-warning']) !!}
            {!! Form::text('paid_commission', $user->paid_commission, ['class' => 'form-control',]) !!}

            {!! Form::submit('提交', ['class' => 'form-control btn btn-success']) !!}

            {!! Form::close() !!}
        </div>




@endsection