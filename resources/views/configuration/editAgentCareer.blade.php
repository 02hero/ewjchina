@extends('layouts.admin')

@section('meta_title','置业顾问置业等级')

@section('content')

    <h2 class="text-center text-success"> 说明:{{$configuration->description}}</h2>

    {!! Form::model($configuration, ['url' => ['configuration/update',$configuration], 'method' => 'post']) !!}
    {!! Form::label('value', '以英文逗号分隔,收尾不需要英文逗号', ['class' => 'control-label']) !!}
    {!! Form::text('value',$configuration->value, ['class' => 'form-control','id'=>'editor','placeholder'=>'ok is me','autofocus']) !!}
    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
    {!! Form::close() !!}

@endsection

@section('script')
    <script>

        $('#conf_agent_career').addClass("active");

    </script>
@endsection


