@extends('layouts.admin')

@section('meta_title','置业阶段配置')

@section('content')



    <h2 class="text-center text-success"> 置业阶段配置说明:{{$configuration->description}}</h2>


    {!! Form::model($configuration, ['url' => ['configuration/update',$configuration], 'method' => 'post']) !!}
    {!! Form::text('value',$configuration->value, ['class' => 'form-control','id'=>'editor','placeholder'=>'ok is me','autofocus']) !!}
    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
    {!! Form::close() !!}


@endsection

@section('script')
    <script>




        $('#conf_sale_stage').addClass("active");

    </script>
@endsection


