@extends('layouts.admin')

@section('meta_title','活动规则编辑')

@section('content')

    <h2 class="text-center text-success"> 活动规则:{{$configuration->description}}</h2>
    <a href="{{url('rule')}}" target="_blank" class="btn btn-success text-right">效果预览</a>

    {!! Form::model($configuration, ['url' => ['configuration/update',$configuration], 'method' => 'post']) !!}
    {!! Form::textarea('value',$configuration->value, ['class' => 'form-control','id'=>'editor','placeholder'=>'ok is me','autofocus']) !!}
    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
    {!! Form::close() !!}

@endsection

@section('script')
    <script>


        var editor = new Simditor({
            textarea: $('#editor'),
            upload: false,
            tabIndent: true,
            toolbar:['title', 'bold', 'italic', 'underline', 'strikethrough', 'fontScale', 'color', 'ol', 'ul', 'blockquote', 'code','link','hr', 'indent', 'outdent', 'alignment',],
        });


        $('#conf_activity_rule').addClass("active");



    </script>
@endsection


