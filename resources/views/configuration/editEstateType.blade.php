@extends('layouts.admin')

@section('meta_title','物业类型配置')

@section('content')

    <h2 class="text-center text-success"> 物业类型说明:{{$configuration->description}}</h2>

    {!! Form::model($configuration, ['url' => ['configuration/update',$configuration], 'method' => 'post']) !!}
    {!! Form::text('value',$configuration->value, ['class' => 'form-control','id'=>'editor','placeholder'=>'ok is me','autofocus']) !!}
    {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
    {!! Form::close() !!}


@endsection

@section('script')
    <script>



        $('#conf_estate_type').addClass("active");

    </script>
@endsection


