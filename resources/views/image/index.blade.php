@extends('layouts.admin')


@section('meta_title','七牛云图片')


@section('content')



    <h2 class="text-center text-success"> 七牛云图片</h2>

    <table class="table table-striped table-hover table-responsive table-condensed table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>图片</th>
            <th>hash(值)</th>
            <th>尺寸</th>
            <th>创建时间</th>
            <th>操作</th>
        </tr>
        </thead>


        <tbody>
        @foreach($images  as $image)
            <tr>
                <td>{{$image['key']}}</td>
                <td>
                    <img src="{{config('app.QNURL').$image['key'].'?imageView2/2/h/100/interlace/1/q/100|watermark/2/text/ReWutue7j-e6quS6ug==/font/5a6L5L2T/fontsize/50/fill/I0RGMTYxNg==/dissolve/100/gravity/North/dy/10
'}}" class="img-responsive" alt="Responsive image">



                    </td>
                <td>{{$image['hash']}}</td>
                <td>{{$image['fsize']}}</td>
                <td>{{$image['putTime']}}</td>

                <td>
                    {{--<a class="btn btn-warning" href="{{url('admin/editConfiguration',$conf->id)}}">编辑</a>--}}
                </td>
            </tr>
        @endforeach

        </tbody>


    </table>



@endsection


@section('script')
    <script>
        $(function(){
            $('#image').addClass("active");
        });
    </script>
@endsection