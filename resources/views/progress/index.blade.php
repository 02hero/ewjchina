@extends('layouts.admin')


@section('meta_title','全部客户进度列表')


@section('content')

    <h2 class="text-center text-success">全部客户购房进度列表</h2>
    <p class="text-right"><a class="btn btn-success" href="{{route('progress.create')}}">录入客户购房进度</a> </p>

    <table class="table table-striped table-hover table-responsive table-condensed table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>客户ID</th>
            <th>客户姓名</th>
            <th>职业顾问</th>
            <th>购房进度</th>
            <th>进度时间</th>
            <th>更新时间</th>
            <th>操作</th>
        </tr>
        </thead>

        <tbody>
        @foreach($progresses as $progress)
            <tr>
                <td>{{$progress->id}}</td>
                <td>{{$progress->client_id}}</td>
                <td>{{$progress->client->name}}</td>
                <td>{{$progress->client->user->name}}</td>
                <td>{{$progress->stage}}</td>
                <td>{{$progress->stage_time}}</td>
                <td>{{$progress->updated_at}}</td>
                <td>
                    {{--<a class="btn btn-primary" href="{{route('progress.edit',$progress)}}">编辑</a>--}}
                    {!! Form::model($progress, ['route' => ['progress.destroy', $progress], 'method' => 'delete']) !!}
                    {!! Form::submit('删除', ['class' => 'form-control btn-danger']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach

        </tbody>



        </thead>
    </table>


@endsection



@section('script')
    <script>
        $(function(){

            $('#progress').addClass("active");
        });
    </script>
@endsection