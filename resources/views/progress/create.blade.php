@extends('layouts.admin')


@section('meta_title','添加购房进度')


@section('content')
    <h2 class="text-center">添加购房者进度信息</h2>
    <div class="well-lg">
        {!! Form::open(['route' => 'progress.store', 'method' => 'post']) !!}

        {!! Form::label('client_id', '职业顾问姓名', ['class' => 'control-label']) !!}
        {!! Form::select('client_id', $clients , null , ['class' => 'form-control']) !!}

        {!! Form::label('stage', '购房进度描述', ['class' => 'control-label text-danger','placeholder'=>'万科城花']) !!}
        {!! Form::select('stage', $stages , null , ['class' => 'form-control']) !!}

        {!! Form::label('stage_time', '购房进度时间', ['class' => 'control-label text-danger']) !!}
        {!! Form::date('stage_time' , null , ['class' => 'form-control']) !!}

        {!! Form::submit('Submit', ['class' => 'form-control btn-success']) !!}
        {!! Form::close() !!}
    </div>
@endsection



@section('script')
    <script>
        $(function(){
            $('#progress').addClass("active");
        });
    </script>
@endsection