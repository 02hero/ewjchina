<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pid')->nullable();
            $table->unsignedTinyInteger('rank')->default(9);
            $table->string('agent_type')->nullable();
            $table->string('name')->nullable();
            $table->string('company')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone',11)->unique()->nullable();
            $table->string('identity_id',18)->unique()->nullable();
            $table->enum('gender',['男','女'])->default('男');
            $table->string('address')->nullable();
            $table->string('password')->nullable();
            $table->string('wechat_openid')->nullable();;
            $table->string('wechat_token')->nullable();
            $table->string('wechat_avatar')->nullable();
            $table->string('wechat_name')->nullable();
            $table->unsignedInteger('paid_commission')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
