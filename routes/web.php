<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/load', function () {
    return redirect('/');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/coupon', 'AgentController@coupon');
Route::get('/custom', 'AgentController@custom');
Route::get('/agent', 'AgentController@index');//index
Route::get('/', 'AgentController@load');
Route::get('/commission', 'AgentController@commission');
Route::get('/reg', 'AgentController@reg');
Route::post('/saveAgentInfo', 'AgentController@saveAgentInfo');

Route::get('/rule', 'AgentController@rule');
Route::get('/share', 'AgentController@share');


Route::group(['prefix' => 'admin'], function () {

    Route::get('/','UserController@userList');
    Route::get('/userEdit/{user}','UserController@userEdit');
    Route::post('/userUpdate','UserController@userUpdate');

});


Route::get('/wechatcheck','WechatController@validateWechatToken');
Route::any('/wechatLoginCallback','WechatController@wechatLoginCallback');


Route::resource('client','ClientController');
Route::get('/myClients', 'ClientController@myClients');//对应register view
Route::get('/clientAdd', 'ClientController@clientAdd');//对应register view
Route::post('/clientInfoSave', 'ClientController@clientInfoSave');//对应register view


Route::resource('progress','ProgressController',['except'=>['show','edit']]);
Route::resource('property','PropertyController',['except'=>['show','update']]);
Route::get('property/uploadImage/{property}','PropertyController@uploadImage');
Route::post('property/storeImage','ImageController@storeImage');


Route::resource('image','ImageController',['except'=>['show','update','create','store','destroy']]);


Route::get('configuration/editEstateType','ConfigurationController@editEstateType');
Route::get('configuration/editSaleStage','ConfigurationController@editSaleStage');
Route::get('configuration/editActivityRule','ConfigurationController@editActivityRule');
Route::get('configuration/editSaleStage','ConfigurationController@editSaleStage');
Route::get('configuration/editAgentCareer','ConfigurationController@editAgentCareer');
Route::post('configuration/update/{configuration}','ConfigurationController@update');


