$(function(){
    $par = $('.register');
    $par.on('click','.register-tab span',function(){
        var $obj = $(this);
        $('.register-tab span').removeClass('register-tab-active');
        $obj.addClass('register-tab-active');
        $('.register-form li input').val('');
        if($obj.data('type') == 1){
            $('.register-form-2').hide();
            $('.register-form-1').show();
        }else{
            $('.register-form-1').hide();
            $('.register-form-2').show();
        }
    });
});
