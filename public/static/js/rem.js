(function(){
    var docEl = document.documentElement,
       resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
       recalc = function() {
           docEl.style.fontSize = 20 * (docEl.clientWidth / 650) + 'px';
       };
   window.addEventListener(resizeEvt, recalc, false);
   document.addEventListener('DOMContentLoaded', recalc, false);
})();
