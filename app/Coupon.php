<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'title','body',
        'image','property_id',
        'old_price','new_price',
        'user_id'
    ];


    public function property() {
        return $this->belongsTo('App\Property');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
