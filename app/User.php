<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'pid',
        'agent_type',
        'rank','company',
        'identity_id',
        'address',
        'phone',
        'gender',
        'wechat_openid',
        'wechat_token',
        'wechat_avatar',
        'wechat_name',
        'paid_commission',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function clients () {
        return $this->hasMany('App\Client');
    }


    public function coupons () {
        return $this->hasMany('App\Coupons');
    }



}
