<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = [
        'user_id','name','phone','gender',
        'property_id','intention_area',
        'intention_price','intention_type','intention_apartment',
        'remark','is_finished','is_recommanding'
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function progresses () {
        return $this->hasMany('App\Progress');
    }

    public function property() {
        return $this->belongsTo('App\Property');
    }


}
