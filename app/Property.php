<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'title','detail',
        'coverage_img',
        'images','city_area',
        'address',
        'feature','price',
        'commission',
        'commission_title',
        'commission_detail'
    ];

    public function coupons () {
        return $this->hasMany('App\Coupon');
    }

    public function getCoverageUrlAttribute() {
        return config('app.QNURL').$this->coverage_img;
    }



    public function getCoverageH100Attribute() {
        $para = '?imageView2/2/h/200/interlace/1/q/100|watermark/2/text/ReWutue7j-e6quS6ug==/font/5a6L5L2T/fontsize/50/fill/I0RGMTYxNg==/dissolve/100/gravity/North/dy/10
';
        return config('app.QNURL').$this->coverage_img.$para;
    }

    public function clients() {
        return $this->hasMany('App\Client');
    }
}
