<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function userList()
    {
        $agents = User::all();
        return view('admin.listAgent',compact('agents'));
    }


    public function userEdit(User $user)
    {
        return view('admin.editAgent',compact('user'));
    }

    public function userUpdate(Request $request)
    {
        try {
            $user = User::find($request->input('id'));
            $user->update($request->all());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        Session::flash('success','更新成功');
        return redirect(url('admin'));
    }

}
