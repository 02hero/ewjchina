<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Progress;
use App\Client;

class ProgressController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {
        $progresses = Progress::all();
        return view('progress.index',compact('progresses'));
    }


    public function create() {
        $clients = Client::all()->pluck('name','id')->toArray();
        $progress = Configuration::whereKey('client_progress')->first()->value;
        $progress = explode(',',$progress);
        foreach ($progress as $p) {
            $stages[$p] = $p;
        }
        return view('progress.create',compact('stages','clients'));
    }

    public function store(Request $request) {
        Progress::create($request->all());
        return redirect(route('progress.index'));
    }
    public function destroy(Progress $progress) {
        $progress->delete();
        return redirect(route('progress.index'));
    }


}
