<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Configuration;
use App\Property;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('load');
    }

    public function load()
    {
        //微信第三方登陆地址
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxa585c7f09efe3ec0&redirect_uri=http://wx.ejwchina.com/wechatLoginCallback&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect';
        return view('agent.load',compact('url'));
    }

    public function coupon()
    {
        return view('agent.coupon');
    }

    public function custom()
    {
        return view('agent.custom');
    }
    public function index()
    {
        $user = Auth::user();
        $properties = Property::where('id','>',1)->get();
        $commission = $user->clients()->with('property')->where('is_finished','>',0)->get()->sum('property.commission');

        //领取微信优惠劵地址
        $shareURL = 'http://mp.weixin.qq.com/bizmall/cardshelf?shelf_id=1&showwxpaytitle=1&biz=MzIzNDQ4NzQyMw==&t=cardticket/shelf_list&scene=1000007#wechat_redirect';
        return view('agent.index',compact('user','properties','shareURL','commission'));
    }

    public function commission()
    {
        $commission = Auth::user()->clients()->with('property')->where('is_finished','>',0)->get()->sum('property.commission');
        $paid_commission = Auth::user()->paid_commission;
        return view('agent.commission',compact('commission','paid_commission'));
    }

    public function reg()
    {
        $conf = Configuration::whereKey('agent_type')->first();
        $agent_types = explode(',',$conf->value);
        return view('agent.reg',compact('agent_types'));
    }


    public function rule()
    {
        $conf = Configuration::where('key','activity_rule')->first();
        return view('agent.rule',compact('conf'));
    }

    public function share()
    {
        $properties = Property::all();
        return view('agent.share',compact('properties'));
    }


    public function saveAgentInfo(Request $request)
    {

        $this->validate($request, [
            'agent_type' => 'required',
            'name'=>'required|max:40',
            'gender' => 'required',
            'phone' => 'required|digits:11',
            'identity_id' => 'required|string|size:18',
        ]);
        $user = Auth::user();
        $data = $request->all();
        $user->update($data);

        return response()->json($user->toArray(),200);
//        return response()->json($request->all(),200);
    }



}

