<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Configuration;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class ConfigurationController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function editActivityRule() {
        $key = 'activity_rule';
        $configuration = Configuration::whereKey($key)->first();
        if(!$configuration) {
            $default = ['key'=>$key,'description'=>'活动规则(支持富文本)','value'=>'请到后台配置活动规则富文本信息'];
            $configuration = Configuration::create($default);
        }
        return view('configuration.editActivityRule',compact('configuration'));
    }
    public function editAgentCareer() {
        $key = 'agent_type';
        $configuration = Configuration::whereKey($key)->first();
        if(!$configuration) {
            $default = ['key'=>$key,'description'=>'顾问类别(文本逗号分隔)','value'=>'普通顾问,高级顾问,职业顾问,兼职置业,总监,超级管理员'];
            $configuration = Configuration::create($default);
        }
        return view('configuration.editAgentCareer',compact('configuration'));
    }
    public function editSaleStage() {
        $key = 'client_progress';
        $configuration = Configuration::whereKey($key)->first();
        if(!$configuration) {
            $default = ['key'=>$key,'description'=>'购房者进度配置(文本英文逗号分隔)','value'=>'预约,看房,参观,定金,签约,按揭,交房,过户'];
            $configuration = Configuration::create($default);
        }
        return view('configuration.editSaleStage',compact('configuration'));
    }
    public function editEstateType() {
        $key = 'property_type';
        $configuration = Configuration::whereKey($key)->first();
        if(!$configuration) {
            $default = ['key'=>$key,'description'=>'楼盘类别(文本逗号分隔)','value'=>'普通住宅,公寓住在,别墅,写字楼'];
            $configuration = Configuration::create($default);
        }
        return view('configuration.editEstateType',compact('configuration'));
    }

    /**
     * 功能钻不实现
     */
    public function editEstateShape() {
        $key = 'property_shape';
        $configuration = Configuration::whereKey($key)->first();
        if(!$configuration) {
            $default = ['key'=>$key,'description'=>'户型选项(英文逗号分隔)','value'=>'二室一厅,三室二厅,三室两厅两卫,通透,人车分离'];
            $configuration = Configuration::create($default);
        }
        return view('configuration.editEstateShape',compact('configuration'));
    }

    public function update(Configuration $configuration,Request $request) {
        $data = $request->all();
        $configuration->update($data);
        Session::flash('success', '配置'.$configuration->key.'修改成功');
        return redirect(URL::previous());
    }


}
