<?php

namespace App\Http\Controllers;

use App\Progress;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Property;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;

class PropertyController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $properties = Property::all();
        return view('property.index',compact('properties'));
    }

    public function create() {
        return view('property.create');
    }

    public function edit(Property $property) {
        //dd(config('app.QNURL'));
        return view('property.edit',compact('property'));
    }

    public function store(Request $request) {
        $id = $request->input('id');
        if($id) {//编辑

            try {
                //commission
                $p = Property::find($id);
                $p->update($request->all());
                $p->save();
            } catch(\Exception $e) {
                Session::flash('error', $e->getMessage());
            }
        } else {//增加

            try {
                Property::create($request->all());
            } catch(\Exception $e) {
                Session::flash('error', $e->getMessage());
            }
        }
        return redirect(route('property.index'));
    }




    public function destroy (Property $property) {
        $res = $property->delete();

        return redirect(action('PropertyController@index'));
    }



    public function uploadImage(Property $property) {
        return view('property.uploadImage',compact('property'));
    }





}
