<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Progress extends Model
{
    protected $fillable = ['stage','client_id','stage_time'];


    public function client() {
        return $this->belongsTo('App\Client');
    }
    public function getStageTimeAttribute($value) {
        return substr($value,0,10);
    }

}
